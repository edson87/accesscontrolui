import React, { Component } from "react";
import {
  StyleSheet,
  SafeAreaView,
  View,
  TextInput,
  Text,
  TouchableHighlight,
  Alert,
} from "react-native";
import axios from "axios";

type MyProps = { navigation: any };
type MyState = { long: any; lat: any };
export default class Login extends Component<MyProps, MyState> {
  constructor(props:any) {
    super(props)
  }
  componentDidMount() {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        this.setState({
          long: position.coords.longitude,
          lat: position.coords.latitude,
        });
      },
      (error) => {
        Alert.alert("No se puede optener los datos con el geolocalizador apagado")
        setTimeout(() => {
          this.props.navigation.navigate('Login')
        }, 3000);    
      },
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
    );
  }

  render() {
    const { navigation } = this.props;
    var hour = new Date().getHours();
    var minutes = new Date().getMinutes();
    var currentHour = hour + ":" + minutes;

    const handleCheckIn = () => {
      /* Alert.alert("Entrada Registrada")

      setTimeout(() => {
        this.props.navigation.navigate('Login')
      }, 3000);  */
      fetch("https://localhost:44320/api/registers", {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          Latitude: this.state.lat.toString(),
          Longitude: this.state.long.toString(),
          EntryTypeId: 1,
        }),
      })
        .then((res) => res.json())
        .then((res) => {
          window.alert('Entrada Registrada')
          Alert.alert("Entrada Registrada")
          setTimeout(() => {
            this.props.navigation.navigate('Login')
          }, 3000); 
        });
    };

    const handleCheckOut = () => {
      /* Alert.alert("Salida registrada")
      
      setTimeout(() => {
        this.props.navigation.navigate('Login')
      }, 3000);  */  
       fetch("https://localhost:44320/api/registers", {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          Latitude: this.state.lat.toString(),
          Longitude: this.state.long.toString(),
          EntryTypeId: 2,
        }),
      })
        .then((res) => res.json())
        .then((res) => {
          window.alert('Salida Registrada')
          Alert.alert("Salida Registrada")
          setTimeout(() => {
            this.props.navigation.navigate('Login')
          }, 3000);   
        });
    };

    return (
      <View style={styles.content}>
        <Text style={styles.hour}>{currentHour}</Text>
        <SafeAreaView>
          <TouchableHighlight
            style={styles.button}
            onPress={handleCheckIn}
            underlayColor="#99d9f4"
          >
            <Text style={styles.buttonText}>Entrada</Text>
          </TouchableHighlight>
          <TouchableHighlight
            style={styles.button}
            onPress={handleCheckOut}
            underlayColor="#99d9f4"
          >
            <Text style={styles.buttonText}>Salida</Text>
          </TouchableHighlight>
        </SafeAreaView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  content: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  hour: {
    fontSize: 70,
    paddingBottom: 20,
    color: "blue",
  },
  buttonText: {
    fontSize: 18,
    color: "white",
    alignSelf: "center",
  },
  button: {
    height: 36,
    backgroundColor: "#48BBEC",
    borderColor: "#48BBEC",
    borderWidth: 1,
    borderRadius: 8,
    alignSelf: "stretch",
    justifyContent: "center",
    width: 200,
    margin: 10,
  },
});
