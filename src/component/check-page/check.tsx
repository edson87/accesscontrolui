import React, { Component } from 'react';
import { StyleSheet, SafeAreaView, View, TextInput, Text, TouchableHighlight, Alert } from 'react-native';

type MyProps = { navigation: any, route: any};
type MyState = { long: any, lat: any };
export default class Check extends Component<MyProps, MyState> {
  constructor(props: any) {
    super(props);
    this.state = {
        long: '',
        lat: ''
     };
  }
  componentDidMount() {
		navigator.geolocation.getCurrentPosition(
			position => {
				//const location = JSON.stringify(position);
				this.setState({ long: position.coords.longitude, lat: position.coords.latitude });
			},
			error => Alert.alert(error.message),
			{ enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
		);

  }

  render() {
    const { navigation } = this.props;
    const { enteryTypeId } = this.props.route.params
    
    var hour = new Date().getHours();
    var minunt = new Date().getMinutes();
    var seconds = new Date().getSeconds();

    const go = () => {
       
        console.log(this.state)
    }

    return (
      <View style={styles.content} >
       <Text>Usted esta registrando su {enteryTypeId==1? 'entrada':'salida'} a horas {hour} : {minunt} </Text>

       <View style={styles.buttonContent}>
        <TouchableHighlight style={styles.button} onPress={go} underlayColor='#99d9f4'>
          <Text style={styles.buttonText}>Check Out</Text>
        </TouchableHighlight>
       </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    content: {
        /*flex: 1,
        backgroundColor: '#fff',*/
        alignItems: 'center',
        /*justifyContent: 'center',*/
    },
    buttonText: {
      fontSize: 18,
      color: 'white',
      alignSelf: 'center'
    },
    buttonContent: {
      display: 'flex',
      justifyContent: 'center'
    },
    button: {
      height: 36,
      backgroundColor: '#48BBEC',
      borderColor: '#48BBEC',
      borderWidth: 1,
      borderRadius: 8,
      alignSelf: 'stretch',
      justifyContent: 'center',
      width: 200,
      margin: 10
    }
  })