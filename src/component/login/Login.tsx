import React, { Component } from "react";
import {
  StyleSheet,
  SafeAreaView,
  View,
  TextInput,
  Text,
  TouchableHighlight,
  Alert,
} from "react-native";

type MyProps = { navigation: any };
type MyState = { username: string; password: string };
export default class Login extends Component<MyProps, MyState> {
  constructor(props: any) {
    super(props);
    this.state = {
      username: "",
      password: "",
    };
  }

  /*const [login, onChangeLogin] = React.useState("");
  const [password, onChangePassword] = React.useState("");*/

  render() {
    const { navigation } = this.props;

    const onChangeUsername = (username: string) => {
      this.setState({ username });
    };

    const onChangePassword = (password: any) => {
      this.setState({ password });
    };

    const onPress = () => {
      if (this.state.username == "admin" && this.state.password == "pass") {
        console.log(this.state);
        this.setState({
          username: "",
          password: "",
        });
        navigation.navigate("Home");
      } else {
        Alert.alert("Cuenta incorrecta ingrese de nuevo");
      }
    };

    return (
      <View style={styles.containerLogin}>
        <View style={styles.titleContent}>
          <Text style={styles.title}>Ingrese su cuenta</Text>
        </View>
        <SafeAreaView>
          <TextInput
            {...this.props}
            style={styles.input}
            onChangeText={(username) => onChangeUsername(username)}
            value={this.state.username}
            placeholder="Usuario"
            keyboardType="default"
          />
          <TextInput
            style={styles.input}
            onChangeText={(password) => onChangePassword(password)}
            value={this.state.password}
            placeholder="Contraseña"
            keyboardType="default"
            secureTextEntry={true}
          />
          <View style={styles.buttonContent}>
            <TouchableHighlight
              style={styles.button}
              onPress={onPress}
              underlayColor="#99d9f4"
            >
              <Text style={styles.buttonText}>Ingresar</Text>
            </TouchableHighlight>
          </View>
        </SafeAreaView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  titleContent: {
    //width: 200
  },
  title: {
    alignItems: "center",
    color: "black",
    fontSize: 20,
    fontWeight: "bold",
  },
  containerLogin: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    width: 250,
    borderRadius: 15,
    color: "cornflowerblue",
    ooutline: "none"
  },
  buttonContent: {},
  buttonText: {
    fontSize: 18,
    color: "white",
    alignSelf: "center",
  },
  button: {
    height: 36,
    backgroundColor: "#48BBEC",
    borderColor: "#48BBEC",
    borderWidth: 1,
    borderRadius: 8,
    marginBottom: 10,
    alignSelf: "stretch",
    justifyContent: "center",
    width: 200,
    margin: 10,
    marginLeft: 40,
  },
});

//export default Login;
